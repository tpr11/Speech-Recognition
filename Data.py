import os
import glob
import random
import pickle

import numpy as np
import scipy.io.wavfile
import scipy.signal
import librosa.effects

def separate_sets():
    base_dir = 'data/train'
    testing_list = open('%s/testing_list.txt' % (base_dir), 'r').read().split('\n')[:-1]
    validation_list = open('%s/validation_list.txt' % (base_dir), 'r').read().split('\n')[:-1]
    training_list = []

    os.chdir('%s/audio/' % (base_dir))
    dirs = glob.glob('*')
    for d in dirs:
        if d is '_background_noise_': continue
        sound_files = filter(lambda f: f not in testing_list and f not in validation_list, glob.glob('%s/*.wav' % (d)))
        training_list += sound_files
    os.chdir('../../..')

    output_dirs = ['train', 'test', 'validate']
    for d in dirs:
        for output_dir in output_dirs:
            os.makedirs('%s/%s/%s/%s' % (os.getcwd(), base_dir, output_dir, d))

    for l, target_dir in zip([training_list, testing_list, validation_list], output_dirs):
        print('%d files for %s' % (len(l), target_dir))
        for f in l:
            os.rename('%s/%s/audio/%s' % (os.getcwd(), base_dir, f), '%s/%s/%s/%s' % (os.getcwd(), base_dir, target_dir, f))

class Data:

    class Sets:
        TRAIN = 0
        VALIDATION = 1
        TEST = 2

    def __init__(self, base_dir, input_shape, classes, data_augmentation=False):
        if os.path.isfile('data/data.pkl'):
            with open('data/data.pkl', 'rb') as f:
                self.__dict__ = pickle.load(f)
            return
        self.classes = classes
        self.data = {Data.Sets.TRAIN: {}, Data.Sets.VALIDATION: {}, Data.Sets.TEST: {}}
        augmentation_noise = []
        os.chdir(base_dir)
        for data_set, output_dir in zip([Data.Sets.TRAIN, Data.Sets.VALIDATION, Data.Sets.TEST], ['train', 'test', 'validate']):
            os.chdir(output_dir)
            d = self.data[data_set]
            clss = glob.glob('*')
            for c in clss:
                os.chdir(c)
                sound_files = glob.glob('*.wav')
                if c == '_background_noise_':
                    c = 'silence'
                elif c not in classes:
                    c = 'unknown'
                d[c] = []
                if c == 'silence':
                    for sound_file in sound_files:
                        _, samples = scipy.io.wavfile.read(sound_file)
                        samples = [samples[i:i+input_shape] for i in range(0, samples.size, input_shape)]
                        for s in samples:
                            if s.size == input_shape:
                                augmentation_noise.append(s)
                                d[c].append(s)
                else:
                    for sound_file in sound_files:
                        _, samples = scipy.io.wavfile.read(sound_file)
                        samples = samples[:input_shape]
                        if samples.size == input_shape:
                            d[c].append(samples)
                random.shuffle(d[c])
                d[c] = np.array(d[c], dtype=np.float)
                os.chdir('..')
            os.chdir('..')
        if data_augmentation:
            for c in self.data[Data.Sets.TRAIN]:
                if c == 'silence':
                    continue
                new_samples = []
                for i in range(d[c].shape[0]):
                    r_noise = random.sample(augmentation_noise, 10)
                    for j in range(len(r_noise)):
                        new_samples.append(d[c][i] + 0.005 * r_noise[j])
                    r_shift = np.random.randint(40, 4000)
                    new_samples.append(np.pad(d[c][i], ((r_shift, 0)), mode='constant')[:-r_shift])
                    r_stretch = 0.8 + np.random.random() * 0.4
                    if r_stretch <= 0.95 or r_stretch >= 1.05:
                        new_samples.append(np.pad(librosa.effects.time_stretch(d[c][i], r_stretch), ((0, 3200)), mode='constant')[:input_shape])
                    r_pitch = np.random.randint(0, 9) - 4
                    if r_pitch != 0:
                        new_samples.append(librosa.effects.pitch_shift(d[c][i], 16000, r_pitch))
                self.data[Data.Sets.TRAIN][c] = np.concatenate([self.data[Data.Sets.TRAIN][c], new_samples], axis=0)
        self.samples = {
            Data.Sets.TRAIN: sum([self.data[Data.Sets.TRAIN][c].shape[0] for c in self.data[Data.Sets.TRAIN]]),
            Data.Sets.VALIDATION: sum([self.data[Data.Sets.VALIDATION][c].shape[0] for c in self.data[Data.Sets.VALIDATION]]),
            Data.Sets.TEST: sum([self.data[Data.Sets.TEST][c].shape[0] for c in self.data[Data.Sets.TEST]])
        }
        os.chdir('../..')
        with open('data/data.pkl', 'wb') as f:
            pickle.dump(self.__dict__, f)

    def get_all(self, which):
        data = [self.data[which][c] for c in self.data[which] if self.data[which][c].shape[0] != 0]
        labels = [np.ones((self.data[which][c].shape[0],)) * self.classes[c] for c in self.data[which] if self.data[which][c].shape[0] != 0]
        return np.concatenate(data, axis=0), np.concatenate(labels, axis=0)

    def get_batch(self, which, batch_size):
        f = 1.0 * batch_size / self.samples[which]
        ff = {}
        for c in self.data[which]:
            ff[c] = int(f * self.data[which][c].shape[0])
        i = 0
        while i < self.samples[which]:
            data =  np.concatenate([self.data[which][c][i:i+ff[c]] for c in self.data[which] if self.data[which][c].shape[0] != 0], axis=0)
            labels = np.concatenate([np.ones((ff[c],)) * self.classes[c] for c in self.data[which] if self.data[which][c].shape[0] != 0], axis=0)[:data.shape[0]]
            i += data.shape[0]
            if data.shape[0] < len(self.classes):
                break
            yield data, labels
        for c in self.data[which]:
            p = np.random.permutation(self.data[which][c].shape[0])
            self.data[which][c] = self.data[which][c][p]