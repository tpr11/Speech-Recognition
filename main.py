import os
import glob

from Model import Model
from Data import Data

import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    classes = 'yes no up down left right on off stop go silence unknown'.split(' ')
    num_classes = len(classes)
    classes = dict([(e,i) for (i,e) in enumerate(classes)])
    input_dim = 16000
    epochs = 500
    batch_size = 128

    data = Data('data/train', input_dim, classes)

    model = Model(input_dim, num_classes, learning_rate=1e-3, regularization=1e-3)
    
    losses, accs = ([], []), ([], [])
    best_loss = None
    try:
        for i in range(epochs):
            training_loss, training_acc = [], []
            for training_data, training_labels in data.get_batch(Data.Sets.TRAIN, batch_size):
                model.fit(training_data, training_labels)
                loss, accuracy = model.metrics(training_data, training_labels)
                training_loss.append(loss)
                training_acc.append(accuracy)
            training_loss, training_acc = np.mean(training_loss), np.mean(training_acc)
            validation_loss, validation_acc = [], []
            for validation_data, validation_labels in data.get_batch(Data.Sets.VALIDATION, batch_size):
                loss, accuracy = model.metrics(validation_data, validation_labels)
                validation_loss.append(loss)
                validation_acc.append(accuracy)
            validation_loss, validation_acc = np.mean(validation_loss), np.mean(validation_acc)
            print('EPOCH %d - train_loss %.6f - train_acc %.4f - val_loss %.6f - val_acc %.4f' % (i, training_loss, training_acc, validation_loss, validation_acc))
            losses[0].append(training_loss)
            losses[1].append(validation_loss)
            accs[0].append(training_acc)
            accs[1].append(validation_acc)
            if not best_loss or validation_loss < best_loss:
                best_loss = validation_loss
            	model.save_model('models/model-%.6f' % (validation_loss))
    except KeyboardInterrupt as kie:
        print('Ctrl+C')

    fig, ax = plt.subplots(1, 2, figsize=(30, 10))

    l_t, = ax[0].plot(np.arange(len(losses[0])), losses[0], 'b')
    l_v, = ax[0].plot(np.arange(len(losses[1])), losses[1], 'g')
    ax[0].plot(np.arange(len(losses[0])), np.ones((len(losses[0]),)) * np.min(losses[0]), 'b', linestyle='--')
    ax[0].plot(np.arange(len(losses[1])), np.ones((len(losses[1]),)) * np.min(losses[1]), 'g', linestyle='--')
    ax[0].set_ylabel('loss')
    ax[0].set_xlabel('epochs')
    ax[0].set_ylim(bottom=0)
    ax[0].set_xlim([0, len(losses[0])-1])
    ax[0].set_yticks(list(ax[0].get_yticks()) + [np.min(losses[0]), np.min(losses[1])])

    ax[1].plot(np.arange(len(accs[0])), accs[0], 'b')
    ax[1].plot(np.arange(len(accs[1])), accs[1], 'g')
    ax[1].plot(np.arange(len(accs[0])), np.ones((len(accs[0]),)) * np.max(accs[0]), 'b', linestyle='--')
    ax[1].plot(np.arange(len(accs[1])), np.ones((len(accs[1]),)) * np.max(accs[1]), 'g', linestyle='--')
    ax[1].set_ylabel('accuracy')
    ax[1].set_xlabel('epochs')
    ax[1].set_ylim([0.0, 1.001])
    ax[1].set_xlim([0, len(accs[0])-1])
    ax[1].set_yticks(filter(lambda i: np.abs(i-np.max(accs[0])) >= 0.01 and np.abs(i-np.max(accs[1])) >= 0.01, list(np.arange(100, step=5) / 100.0)) + [np.max(accs[0]), np.max(accs[1])])

    fig.legend((l_t, l_v), ('Training', 'Validation'), 'upper right', fontsize='x-large')
    fig.savefig('metrics.png')