package com.example.tpr.sar.Model;

import java.util.Objects;

public class LocalFile {
    private String path;
    private FileType type;
    private Long size;

    public LocalFile(String path) {
        this.path = path;
        this.type = FileType.OTHERS;
    }

    public LocalFile(String path, FileType type) {
        this.path = path;
        this.type = type;
    }

    public LocalFile(String path, FileType type, Long size) {
        this.path = path;
        this.type = type;
        this.size = size;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public FileType getType() {
        return type;
    }

    public void setType(FileType type) {
        this.type = type;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocalFile file = (LocalFile) o;
        return Objects.equals(path, file.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path);
    }
}
