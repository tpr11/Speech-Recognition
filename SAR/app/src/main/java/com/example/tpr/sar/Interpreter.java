package com.example.tpr.sar;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.util.concurrent.locks.ReentrantLock;

public class Interpreter {

    private static Interpreter instance;
    private Interpreter() {}
    public static Interpreter getInstance() {
        if (instance == null) {
            synchronized (Interpreter.class) {
                if (instance == null) {
                    instance = new Interpreter();
                }
            }
        }
        return instance;
    }

    private static final String[] LABELS = { "yes", "no", "up", "down", "left", "right", "on", "off", "stop", "go", "silence", "unknown" };
    private static final String INPUT_NAME = "inputs";
    private static final String OUTPUT_NAME = "predictions";
    private static final String MODEL_NAME = "model.pb";
    private static final int SAMPLE_RATE = 16000;
    private static final int SAMPLE_DURATION_MS = 1000;
    private static final int RECORDING_LENGTH = (int) (SAMPLE_RATE * SAMPLE_DURATION_MS / 1000);
    private static final long AVERAGE_WINDOW_DURATION_MS = 500;
    private static final float DETECTION_THRESHOLD = 0.70f;
    private static final int SUPPRESSION_MS = 1500;
    private static final int MINIMUM_COUNT = 3;
    private static final long MINIMUM_TIME_BETWEEN_SAMPLES_MS = 30;

    private Thread recordingThread;
    private Thread recognitionThread;
    private boolean continueRecording = false;
    private boolean continueRecognition = false;
    private final ReentrantLock recordingBufferLock = new ReentrantLock();
    private short[] recordingBuffer = new short[RECORDING_LENGTH];
    private int recordingOffset = 0;

    private TensorFlowInferenceInterface inferenceInterface;

    public synchronized void start(Context context) {

        inferenceInterface = new TensorFlowInferenceInterface(context.getAssets(), MODEL_NAME);

        if (recordingThread == null) {
            continueRecording = true;
            recordingThread = new Thread(this::record);
            recordingThread.start();
        }

        if (recognitionThread == null) {
            continueRecognition = true;
            recognitionThread = new Thread(this::recognize);
            recognitionThread.start();
        }
    }

    private synchronized void record() {

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);

        int bufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
        if (bufferSize == AudioRecord.ERROR || bufferSize == AudioRecord.ERROR_BAD_VALUE) {
            bufferSize = 2 * SAMPLE_RATE;
        }

        short[] audioBuffer = new short[bufferSize / 2];

        AudioRecord record = new AudioRecord(
                MediaRecorder.AudioSource.DEFAULT,
                SAMPLE_RATE,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                bufferSize
        );
        if (record.getState() != AudioRecord.STATE_INITIALIZED) {
            return;
        }

        record.startRecording();

        while (continueRecording) {

            int numberRead = record.read(audioBuffer, 0, audioBuffer.length);
            int maxLength = recordingBuffer.length;
            int newRecordingOffset = recordingOffset + numberRead;
            int secondCopyLength = Math.max(0, newRecordingOffset - maxLength);
            int firstCopyLength = numberRead - secondCopyLength;
            recordingBufferLock.lock();
            try {
                System.arraycopy(audioBuffer, 0, recordingBuffer, recordingOffset, firstCopyLength);
                System.arraycopy(audioBuffer, firstCopyLength, recordingBuffer, 0, secondCopyLength);
                recordingOffset = newRecordingOffset % maxLength;
            } finally {
                recordingBufferLock.unlock();
            }

        }

        record.stop();
        record.release();
    }

    private void recognize() {

        short[] inputBuffer = new short[RECORDING_LENGTH];
        float[] floatInputBuffer = new float[RECORDING_LENGTH];
        float[] outputScores = new float[LABELS.length];
        String[] outputScoresNames = new String[] {OUTPUT_NAME};
        int[] sampleRateList = new int[] {SAMPLE_RATE};

        while (continueRecognition) {
            recordingBufferLock.lock();
            try {
                int maxLength = recordingBuffer.length;
                int firstCopyLength = maxLength - recordingOffset;
                int secondCopyLength = recordingOffset;
                System.arraycopy(recordingBuffer, recordingOffset, inputBuffer, 0, firstCopyLength);
                System.arraycopy(recordingBuffer, 0, inputBuffer, firstCopyLength, secondCopyLength);
            } finally {
                recordingBufferLock.unlock();
            }

            for (int i = 0; i < RECORDING_LENGTH; i++) {
                floatInputBuffer[i] = inputBuffer[i] / 32767.0f;
            }

            inferenceInterface.feed(INPUT_NAME, floatInputBuffer, RECORDING_LENGTH, 1);
            inferenceInterface.run(outputScoresNames);
            inferenceInterface.fetch(OUTPUT_NAME, outputScores);

            System.out.println(outputScores);
        }
    }

}
