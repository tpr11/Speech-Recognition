package com.example.tpr.sar.Activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.example.tpr.sar.Adapters.DirectoryAdapter;
import com.example.tpr.sar.Interpreter;
import com.example.tpr.sar.Model.FileType;
import com.example.tpr.sar.Model.LocalFile;
import com.example.tpr.sar.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DirectoryActivity extends AppCompatActivity {

    private RecyclerView directoryView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter directoryAdapter;

    private String currentDirectoryPath;
    private static final int PERMISSION_REQUEST_CODE = 11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directory);

        if (!Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            System.exit(11);
        }

        if (getIntent() == null || !getIntent().hasExtra("directory")) {
            currentDirectoryPath = Environment.getExternalStorageDirectory().toString();
        }
        else {
            currentDirectoryPath = getIntent().getStringExtra("directory");
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(currentDirectoryPath);
        setSupportActionBar(toolbar);

        directoryView = findViewById(R.id.directory_files);
        directoryView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        directoryView.setLayoutManager(layoutManager);

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[] {Manifest.permission.RECORD_AUDIO}, PERMISSION_REQUEST_CODE);
        }

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }
        else {
            Interpreter.getInstance().start(this);
            createAdapter();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            for (int result : grantResults) {
                if (result != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
            }
            Interpreter.getInstance().start(this);
            createAdapter();
        }
    }

    protected void createAdapter() {
        File currentDirectory = new File(currentDirectoryPath);
        File[] files = currentDirectory.listFiles();
        if (files == null || files.length == 0) {
            return;
        }
        List<LocalFile> localFiles = new ArrayList<>();
        for (File file : files) {
            localFiles.add(new LocalFile(file.getAbsolutePath(), file.isDirectory() ? FileType.DIRECTORY : FileType.OTHERS, file.length()));
        }
        directoryAdapter = new DirectoryAdapter(localFiles);
        directoryView.setAdapter(directoryAdapter);
    }
}
