package com.example.tpr.sar.Adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.tpr.sar.Activities.DirectoryActivity;
import com.example.tpr.sar.Model.LocalFile;
import com.example.tpr.sar.Model.FileType;
import com.example.tpr.sar.R;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;

public class DirectoryAdapter extends RecyclerView.Adapter<DirectoryAdapter.ViewHolder> {

    private List<LocalFile> files;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout layout;
        public ViewHolder(LinearLayout layout) {
            super(layout);
            this.layout = layout;
        }
    }

    public DirectoryAdapter(List<LocalFile> files) {
        this.files = files;
        Collections.sort(this.files, (f1, f2) -> {
            if (f1.getType() == f2.getType())
                return f1.getPath().toLowerCase().compareTo(f2.getPath().toLowerCase());
            return f1.getType().ordinal() - f2.getType().ordinal();
        });
    }

    @Override
    public DirectoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout layout = (LinearLayout) LayoutInflater.from(parent.getContext()).inflate(R.layout.file_view, parent, false);
        ViewHolder vh = new ViewHolder(layout);
        return vh;
    }

    private static String readableFileSize(long size) {
        if(size <= 0) return "0";
        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size)/Math.log10(1000));
        return new DecimalFormat("#,##0.#").format(size/Math.pow(1000, digitGroups)) + " " + units[digitGroups];
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LocalFile file = files.get(position);
        if (position % 2 != files.size() % 2) {
            holder.layout.setBackgroundColor(holder.layout.getResources().getColor(R.color.offWhite, null));
        }
        else {
            holder.layout.setBackgroundColor(holder.layout.getResources().getColor(R.color.white, null));
        }
        ImageView iconView = (ImageView) holder.layout.getChildAt(0);
        TextView nameView = (TextView) holder.layout.getChildAt(1);
        TextView sizeView = (TextView) holder.layout.getChildAt(2);
        if (file.getType() == FileType.DIRECTORY) {
            iconView.setImageResource(R.drawable.ic_folder_black_48dp);
        }
        else {
            iconView.setVisibility(View.GONE);
            nameView.setPadding(nameView.getPaddingTop(), nameView.getPaddingTop(), nameView.getPaddingEnd(), nameView.getPaddingBottom());
            sizeView.setText(readableFileSize(file.getSize()));
        }
        String[] pieces = file.getPath().split("/");
        nameView.setText(pieces[pieces.length - 1]);
        holder.layout.setOnClickListener(v -> clickFile(v, position));
    }

    private void clickFile(View v, int position) {
        LocalFile file = files.get(position);
        if (file.getType() == FileType.DIRECTORY) {
            Intent intent = new Intent(v.getContext(), DirectoryActivity.class);
            intent.putExtra("directory", file.getPath());
            v.getContext().startActivity(intent);
        }
    }

    @Override
    public int getItemCount() {
        return files.size();
    }

}
