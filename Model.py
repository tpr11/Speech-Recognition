import numpy as np
import tensorflow as tf

class Model:

    def __init__(self, input_dim, num_classes, learning_rate, regularization, load_model=None):

        self.session = tf.Session()

        self.x = tf.placeholder(tf.float32, [None, input_dim], name='inputs')
        self.y = tf.placeholder(tf.int64, [None], name='labels')
        self.y_ = tf.one_hot(self.y, depth=num_classes, name='labels_one_hot')
        self.dropout = tf.placeholder(tf.float32, name='dropout')

        stfts = tf.contrib.signal.stft(self.x, frame_length=400, frame_step=400, fft_length=400)
        self.magnitude_spectrograms = tf.abs(stfts)
        linear_to_mel_weight_matrix = tf.contrib.signal.linear_to_mel_weight_matrix(80, self.magnitude_spectrograms.shape[-1].value, 16000, 20.0, 8000.0)
        mel_spectrograms = tf.tensordot(self.magnitude_spectrograms, linear_to_mel_weight_matrix, 1)
        mel_spectrograms.set_shape(self.magnitude_spectrograms.shape[:-1].concatenate(linear_to_mel_weight_matrix.shape[-1:]))
        self.log_mel_spectrograms = tf.log(mel_spectrograms + 1e-6)
        #mfccs = tf.contrib.signal.mfccs_from_log_mel_spectrograms(log_mel_spectrograms)[..., :13]
        
        model = tf.layers.conv2d(tf.expand_dims(self.log_mel_spectrograms, axis=3),
                                 filters=32,
                                 kernel_size=(3,7),
                                 strides=(2,4),
                                 padding='valid',
                                 activation=tf.nn.relu,
                                 kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                                 bias_initializer=tf.ones_initializer(),
                                 kernel_regularizer=tf.contrib.layers.l2_regularizer(regularization))
        model = tf.layers.conv2d(model,
                                 filters=32,
                                 kernel_size=(3,3),
                                 strides=(2,2),
                                 padding='valid',
                                 activation=tf.nn.relu,
                                 kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                                 bias_initializer=tf.ones_initializer(),
                                 kernel_regularizer=tf.contrib.layers.l2_regularizer(regularization))
        model = tf.layers.conv2d(model,
                                 filters=32,
                                 kernel_size=(3,3),
                                 strides=(1,1),
                                 padding='valid',
                                 activation=tf.nn.relu,
                                 kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                                 bias_initializer=tf.ones_initializer(),
                                 kernel_regularizer=tf.contrib.layers.l2_regularizer(regularization))
        model = tf.layers.flatten(model)
        model = tf.layers.dense(model,
                                units=128,
                                activation=tf.nn.relu,
                                kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                                bias_initializer=tf.ones_initializer(),
                                kernel_regularizer=tf.contrib.layers.l2_regularizer(regularization))
        model = tf.layers.dropout(model, rate=self.dropout)
        self.logits = tf.layers.dense(model, 
                                      units=num_classes,
                                      kernel_initializer=tf.truncated_normal_initializer(stddev=0.01),
                                      bias_initializer=tf.ones_initializer(),
                                      kernel_regularizer=tf.contrib.layers.l2_regularizer(regularization),
                                      name='logits')
        self.predictions = tf.nn.softmax(self.logits, name='predictions')

        self.confusion_matrix = tf.confusion_matrix(self.y, tf.argmax(self.predictions, 1))
        self.accuracy = tf.reduce_mean(tf.cast(tf.equal(self.y, tf.argmax(self.predictions, 1)), tf.float32))
        self.loss = tf.losses.softmax_cross_entropy(self.y_, self.logits) + tf.reduce_sum(tf.losses.get_regularization_losses())
        self.optimizer = tf.train.RMSPropOptimizer(learning_rate=learning_rate).minimize(self.loss)

        self.saver = tf.train.Saver(max_to_keep=None)

        self.session.run(tf.global_variables_initializer())  

        if load_model:
            self.saver.restore(self.session, load_model)

    def fit(self, data, labels):
        self.session.run(self.optimizer, feed_dict={self.x: data, self.y: labels, self.dropout: 0.5})

    def predict(self, data):
        return np.argmax(self.session.run(self.predictions, feed_dict={self.x: data, self.dropout: 0}))
    
    def metrics(self, data, labels):
        return self.session.run([self.loss, self.accuracy], feed_dict={self.x: data, self.y: labels, self.dropout: 0})

    def save_model(self, path, step=None):
        self.saver.save(self.session, path, global_step=step)