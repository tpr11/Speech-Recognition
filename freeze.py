import sys

import tensorflow as tf

def freeze(model, output_graph, *args):
    with tf.Session(graph=tf.Graph()) as sess:
        saver = tf.train.import_meta_graph('%s.meta' % (model), clear_devices=True)
        saver.restore(sess, model)
        output_graph_def = tf.graph_util.convert_variables_to_constants(sess, tf.get_default_graph().as_graph_def(), args[0])
        with tf.gfile.GFile(output_graph, 'wb') as f:
            f.write(output_graph_def.SerializeToString())

if __name__ == '__main__':
    freeze(sys.argv[1], sys.argv[2], sys.argv[3:])