import os
import glob
import sys

from Model import Model
from Data import Data

import numpy as np
import matplotlib.pyplot as plt

if __name__ == '__main__':
    classes = 'yes no up down left right on off stop go silence unknown'.split(' ')
    num_classes = len(classes)
    classes = dict([(e,i) for (i,e) in enumerate(classes)])
    input_dim = 16000
    batch_size = 128

    data = Data('data/train', input_dim, classes)

    model = Model(input_dim, num_classes, learning_rate=1e-3, regularization=5e-3, load_model=sys.argv[1])
    
    confusion_matrix = np.zeros((num_classes, num_classes))
    test_loss, test_acc = [], []
    #for test_data, test_labels in data.get_batch(Data.Sets.TEST, batch_size):
    test_data, test_labels = data.get_all(Data.Sets.TEST)
    loss, accuracy = model.metrics(test_data, test_labels)
    test_loss.append(loss)
    test_acc.append(accuracy)
    test_loss, test_acc = np.mean(test_loss), np.mean(test_acc)
    print('LOSS, ACCURACY\t\t%.6f %.4f' % (test_loss, test_acc))
    confusion_matrix = model.session.run(model.confusion_matrix, feed_dict={model.x: test_data, model.y: test_labels})
    norm_conf = confusion_matrix / np.maximum(1e-6, confusion_matrix.sum(axis=1)[:, np.newaxis])
    fig = plt.figure()
    plt.clf()
    ax = fig.add_subplot(111)
    ax.set_title('Confusion Matrix')
    ax.set_aspect(1)
    res = ax.imshow(norm_conf, cmap=plt.cm.jet, interpolation='nearest')
    for x in xrange(num_classes):
        for y in xrange(num_classes):
            ax.annotate(str(confusion_matrix[x, y]), xy=(y, x), 
                        horizontalalignment='center',
                        verticalalignment='center')
    cb = fig.colorbar(res)
    plt.xticks(range(num_classes), classes, rotation=90)
    plt.yticks(range(num_classes), classes)
    plt.savefig('confusion_matrix.png', format='png')